# Marsec: Monadic Parser to convert Markdown into HTML in Haskell
<2023-04-13 Thu>
#+Author: Holychicken

Let's start by asking why we decided to build a parser in a language that only a handful of people have heard of. **Haskell** is a functional language, A paradigm where everything is done by composing functions and applying them.

```bash
 # F1 defines a function that takes an input and adds 1 to it
let F1 = f(a) => a + 1
# F2 defines a function that takes an input and multiplies it by 2
let F2= f(b) => b * 2
# Functions are first class citizens in Functional Languages(They can be passed around as regular arguments). Composing the above functions gives us
F2(F1) == f(c) =>  (c + 1) * 2```
```
Something like this in a purely imperative language i.e. A paradigm that uses a sequence of statements to determine how to reach a certain goal would look like:

```c
// imperative equivalent of F1
int a = scanf("input ")
int result = a + 1
// imperative equivalent of F2
int b = scanf("input")
int result = b * 2
// imperative equivalent of F2(F1)
int c = scanf(input)
int result = (c +1)*2
```
**NOTE**: Over time mortar crumbles, glass shatters and definitions become loose. Ideally in a purely imperative language we would'n use `scanf` but build it ourselves by telling the computer all the steps to take to get the user input but that's a bit overkill for this demonstration.

There is no concept of functions inside Imperative solutions and is more focused on how to *solve the problem* thinking as a computer like defining how to iterate chunks of memory, Functional is more mathematical allowing us to focus more on what the problem is and how to solve it using functional composition

> See this thread on [Y-Combinator](https://www.ycombinator.com/) [Declarative vs Functional](https://news.ycombinator.com/item?id=28350890)

### A Side Quest
Most Universities teaching Computer Science start with `C` going over basics of memory management, basic data structures then go over `C++` giving students a taste of so called **Object Oriented Programming** with encapsulation, polymorphism. This new realm of programming piques their interest and they go online search *Why OOP is good/bad*. They are introduced to a plethora of articles and videos where OOP is Compared against Functional Programming. **Wrong!!** OOP and Functional Programming are not mutually exclusive (atleast by Alan Kay's definition of True OOP and not by the mess `C++` has turned into). Take a look at ~Erlang~, ~Smalltalk~ .
A better question to ask would be the difference between Imperative and Functional.

A **Parser** is a function that takes in input some string and outputs a tree
