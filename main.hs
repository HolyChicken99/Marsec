{-# OPTIONS_GHC -Wall #-}

module Main where

import Control.Applicative
import Data.Char(isAlphaNum,isDigit, toUpper,isLower)

data Title = H1 String | H2 String | H3 String | H4 String deriving (Show,Eq,Read)
-- The AST of the markdown can look
data Block
  = TitleBlock Title ---for multiple titles
  |  Code String --for code blocks
  | Text String --normal paragraph text
  | BlockQuote String -- BlockQuote String starting with >
  | RichText [Richtext]
    deriving (Show)


data Richtext =
  Text' String
  | InlineCode String
  | Link String
  | Bold String
  | Italics String
  | Underlined String
        deriving (Show)

type Markdown = [Block]
-- There's a difference between type and data/Newtype, type is compile time alias while data makes an actual data contructor, Class instances need a data types of kind * -> * and so we use data type to create those data types, using just types would

-- ///NOTE: This implemenatation uses a non-runrecord constructor so will have to pattern match to extact the underlying function
newtype Parser a = P(String-> Maybe[(a,String)])

toUpStr :: String -> String
toUpStr = map toUpper

result :: a -> Parser a
result val = P $ \input -> Just[(val,input)]

item :: Parser Char
item = P(\input -> case input of
            []-> Nothing
            (x:xs) -> Just[(x,xs)])

-- so a functor class defines fmap which can be used to apply a function tp a value inside a container like a list, or Maybe. Here the container is Parser data type and we apply the function to the output value of the parser
instance Functor Parser where
  fmap f (P(d)) = P $ \input -> case d input of
    Nothing -> Nothing
    Just[(v,out)] -> Just[(f v , out)]
    _ -> Nothing

-- define Applicative subclass for parser, the reason we are making this a subclass is with the end goal of using Alternative typeclass `<|>` that replicates BNF `|` or operator, the alternative typeclass is depenedant on these typeclasses

instance Applicative Parser where
  -- pure fun = P(fun)
  -- This wont work because type signature for pure :: a -> f a => takes a type a and returns a parser of type a , in the above declaration we are trying to we are taking a function a -> b and making a parser out of it which wont work because a is type variable and not a function. NOTE: this is not wrong because Parser is defined as String -> ..., we could have returned a function as String -> Maybe[(a->b),String]

  pure v = P(\input -> Just[(v,input)])
 -- (<*>) :: f (a -> b) -> f a -> f b
 -- Parser(a->b) -> Parser (a) -> Parser (b)
  P(pf) <*> px  = P(\input -> case pf input of
                      Nothing -> Nothing
                      Just [(f,out ) ] -> d out
                        where
                          P(d) = fmap f px
                      _ -> Nothing )


instance Monad Parser where
-- (>>=) :: m a -> (a -> m b) -> m b
-- Parser a -> (a -> Parser b ) -> Parser b
-- return :: a ->m a
-- the return word takes a normal value and wraps it in the context
  P(d) >>= f  = P $ \input -> case d input of
                              Nothing -> Nothing
                              Just[(v,out)] -> d out
                                where
                                  P(d) = f v
                              _ -> Nothing


-- The alternative typeclass which replicates the or operator in BNF notation. if one parser fails then then other one is picked.
instance Alternative Parser where
  -- Empty returns a parser that always fails and hence  we return Nothing and not Just[]
  empty = P(\input -> Nothing )
  P(d) <|> P(f) = P $ \input -> d input <|> f input


-- for monads we have a nothing -> nothing rule , the do notation takes note of this and exits when this stage is reached. Anytime we are using Maybe we can use the do-notation because maybe is a monand
-- Takes a string and returns a parser
-- NOTE: // in the future instead of relying on a new function to match strings use the charP predicate to consume strings
stringP :: String -> Parser String
stringP str = P(\input -> case contains str input  of
                   (True,x) -> Just[(str,x)]
                   _ -> Nothing )

-- to find if the second string contains the first one or not
contains :: String -> String -> (Bool,String)
contains "" x  = (True,x)
contains _ ""  = (False,"")
contains x y  = containsHelper x y

containsHelper :: String -> String -> (Bool,String)
containsHelper "" x = (True,x)
containsHelper _ "" = (False,"")
containsHelper str1@(x:xs) str2@(y:ys)= if x == y then containsHelper xs ys else (False,"")


-- Returns a parser that succeds if the predicates is satisfied
sat :: (Char -> Bool) -> Parser Char
sat p = do
          x <- item
          if p x then return x else empty
digit :: Parser Char
digit = sat isDigit

alphanum :: Parser Char
alphanum = sat isAlphaNum

lower :: Parser Char
lower = sat isLower

whitespaces :: Parser Char
whitespaces = sat (==' ')

-- we can use anytype of parser in Parsers like parseTitle as long as the return value is followed, this again is due to return applicative being used to form a new parser. NOTE:// people say *> works as data pipelines to transfer data from operation to another , Wrong ! it creates a new parser so its an emergent behaviour rather than many individual behaviuor

-- ~many~ works by using the same applicative defintion to create a recursive definition that calls itslef if the predicate holds true

ws :: Parser [Char]
ws = many whitespaces

char ::Char -> Parser Char
char x = sat (==x)

-- whitespaces :: Parser String
-- whitespaces = many char ' '
newline :: Parser Char
newline = sat (=='\n')

-- the return value is Parser Block and we are using stringP and other primitives even when they are Parser String, this works becuase we are using the primitives to build a new parser with the return value BLock, the primitives are wrapped inside a parser using <*> to build a new parser returning the required value
parseTitle3 :: Parser Block
parseTitle3 = ws *> stringP "###" *> char ' ' *>  P(\str -> case span (/='\n') str of
                                    ("","") -> Just[(TitleBlock(H3 ""), "")]
                                    ("",_) -> Nothing -- if it could not parse succesfully we fail
                                    (x,xs) -> Just[(TitleBlock (H3 x),xs)] ) <* newline

parseTitle2 :: Parser Block
parseTitle2 = ws *> stringP "##" *>  char ' ' *> P(\str -> case span (/='\n') str of
                                    ("","") -> Just[(TitleBlock(H2 ""),"")]
                                    ("",_) -> Nothing -- if it could not parse succesfully we fail
                                    (x,xs) -> Just[(TitleBlock (H2 x),xs)] ) <* newline

parseTitle1 :: Parser Block
parseTitle1 = ws *> stringP "#" *>  char ' ' *> P(\str -> case span (/='\n') str of
                                    ("","") -> Just[(TitleBlock(H1 ""),"")]
                                    ("",_) -> Nothing -- if it could not parse succesfully we fail
                                    (x,xs) -> Just[(TitleBlock (H1 x),xs)] ) <* newline
parseTitle4 :: Parser Block
parseTitle4 = ws *> stringP "####" *>  char ' ' *> P(\str -> case span (/='\n') str of
                                    ("","") -> Just[(TitleBlock(H4 ""),"")]
                                    ("",_) -> Nothing -- if it could not parse succesfully we fail
                                    (x,xs) -> Just[(TitleBlock (H4 x),xs)] ) <* newline

-- NOTE: The current implementation cant handle text after a single `
parseCode :: Parser Block
parseCode = ws *> stringP "```" *> newline *>  P(\str -> case span (/='`') str of
                                    ("","") -> Just[(Code "","")]
                                    ("",_) -> Nothing -- if it could not parse succesfully we fail
                                    (x,xs) -> Just[( Code x,xs)] ) <* stringP "```" <* newline

-- parse Block Quotes starting with ">"
parseBlockQuote :: Parser Block
parseBlockQuote = ws *> stringP ">" *> P(\input -> case span (/='\n') input of
                                      ("","")->  Just[(BlockQuote "" , "")]
                                      ("",_) -> Nothing
                                      (x,xs) -> Just[(BlockQuote x, xs)])


---- Richtext implementation ---------------------------
parseRichText :: Parser Richtext
parseRichText = parseBold <|> parseItalics <|> parseInlineCode <|> parseUnderlined

parseManyRichText :: Parser [Richtext]
parseManyRichText =  P $ \input -> let P(d)= many parseRichText in case d input of
                                    Just[([],"")] -> Nothing
                                    Just[([],_)] -> Nothing
                                    Just[a] -> Just[a]
                                    _ -> Nothing



parseBold:: Parser Richtext
parseBold = ws *> stringP "**" *> P(\str -> case span(/='*') str of
                             ("","")-> Just[(Bold "","")]
                             ("",_) -> Nothing
                             (x,xs) -> Just[(Bold x, xs)]) <* stringP "**"

parseItalics :: Parser Richtext
parseItalics = ws *> char '*' *> P(\str -> case span(/='*') str of
                             ("","")-> Just[(Italics "","")]
                             ("",_) -> Nothing
                             (x,xs) -> Just[(Italics x, xs)])<* char '*'
parseInlineCode :: Parser Richtext
parseInlineCode =  ws *> char '`' *> P(\str -> case span(/='*') str of
                                   ("","")-> Just[(InlineCode "","")]
                                   ("",_) -> Nothing
                                   (x,xs) -> Just[(InlineCode x , xs)]) <* char '`'

parseUnderlined :: Parser Richtext
parseUnderlined =  ws *>  stringP "__" *> P(\str -> case span(/='_') str of
                             ("","")-> Just[(Underlined "","")]
                             ("",_) -> Nothing
                             (x,xs) -> Just[(Underlined x ,xs)] )
                                           <* stringP "__"


parse :: Parser Block
parse = parseTitle3 <|> parseTitle1 <|> parseTitle2 <|> parseTitle4 <|> parseCode <|> parseBlockQuote <|> (RichText <$> parseManyRichText)

parseMarkdown :: Parser [Block]
parseMarkdown = many parse

-- Note: Make a fuy

main :: IO ()
main = do
  let text = " __tol__ "
  let P(d) =many parse
  let a = d text
  print a

  -- args <- getArgs
  -- case args of
  --   [filepath] -> do
  --     contents <- readFile filepath
  --     let P(d)= parseMarkdown
  --     let  a = d contents
  --     print a

  -- contents <- readFile "temp.txt"

-- so a monad's bind opertaion is  (>>=) :: Maybe a -> (a -> Maybe b) -> Maybe b
