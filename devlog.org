#+title: Devlog
#+Author: Holychicken
*<2023-04-17 Mon>

** ~newtype Parser a = P(String-> Maybe[(a,String)])~ here P is is defined as a data constructor, it creates a new data type unlike newtype which is a type synonym. For typeclasses we need data constructor with kind  *-> * . doing a runrecord sytax like ~newtype Parser a = Parser{parse::String -> Maybe[(a,String)]}~  creates a function ~parse~ as ~parse :: Parser a -> String -> Maybe [(a, String)]~ which can be used to extract the function defined inside the parser.

** The whole reason we are doing this is because its difficult to come up with new names for different types doing the exact same thing, hence we use a generic type constructor to create to functions as part of the generic data structure itself.o /We could have equally made an implementation where instead of using a generic parser type we make different named functions to parse different things/ .. ? (fits well with alternative or other stuff?). Record syntax makes it easier to extract the underlying function so that we dont have to patter match

** Implemented Functor instance for Parser, we need this class for alternative/aplicative and monad ?
* <2023-04-21 Fri>
** finished finals back to dev
** ///NOTE: Fairy tales + dragon

*<2023-04-26 Wed>
** Parser primitives almost done
** Started with parsing richText blocks in markdown
** The alternative Subclass is to handle failures and errors graciously.
*** Finally understood what the *> does, it extracts the function from inside the Parser and using <*> applies it to the second one, so in essence data is not being passed . instead a new Parser is being formed.

* [2023-04-27 Thu]
** Parsers cant handle multiple whitespaces
** Main parser is done , the infinite looping problem was because of using ~many~ which on failure returned and empty list instead of ~Nothing~ so made another wrapping parser that can handle those cases
** Started with actual conversion of the AST to HTML
